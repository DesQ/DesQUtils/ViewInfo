/**
 * This file is a part of DesQ SNI.
 * DesQ SNI is a Widget to show SN Icons for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#pragma once

#include "Global.hpp"

#include <desq/Timer.hpp>

#include <wayqt/WayfireInformation.hpp>
#include <wayqt/LayerShell.hpp>

namespace DesQ {
    class ViewInfo;
}

#if QT_VERSION < QT_VERSION_CHECK( 6, 0, 0 )
    #define QMouseEnterEvent    QEvent
#else
    #define QMouseEnterEvent    QEnterEvent
#endif


class ClickLabel : public QLabel {
    Q_OBJECT

    public:
        ClickLabel( QWidget *parent = nullptr );

        Q_SIGNAL void extendTime();

    private:
        bool mPressed = false;

    protected:
        void mousePressEvent( QMouseEvent *mEvent );
        void mouseReleaseEvent( QMouseEvent *mEvent );
};

class DesQ::ViewInfo : public QWidget {
    Q_OBJECT

    public:
        ViewInfo();
        ~ViewInfo();

        void handleMessages( QString, int );

        void show();
        void hide();

        void close();

    private:
        ClickLabel *mAppIdLbl      = nullptr;
        ClickLabel *mTitleLbl      = nullptr;
        ClickLabel *mUuidLbl       = nullptr;
        ClickLabel *mOutputLbl     = nullptr;
        ClickLabel *mPidLbl        = nullptr;
        ClickLabel *mWorkspaceLbl  = nullptr;
        ClickLabel *mRoleLbl       = nullptr;
        ClickLabel *mGeometryLbl   = nullptr;
        ClickLabel *mIsXwaylandLbl = nullptr;
        ClickLabel *mHasFocusLbl   = nullptr;

        QToolButton *copyBtn = nullptr;

        WQt::LayerSurface *mLyrSurf = nullptr;

        QTimer *hideTimer = nullptr;

        int mClientFD = -1;
        QString mCurrentInfo;

        int mRemainingTime = 0;

        void pauseTimer();
        void resumeTimer();

    protected:
        void enterEvent( QMouseEnterEvent *mEvent );
        void leaveEvent( QEvent *mEvent );

        void paintEvent( QPaintEvent *pEvent );
};

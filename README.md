# DesQ ViewInfo
A simple interface to get information about various views


### Dependencies:
* Qt (Qt5 or Qt6)
* WayQt (https://gitlab.com/desktop-frameworks/wayqt.git)
* DFL Application (https://gitlab.com/desktop-frameworks/applications.git)
* DFL Utils (https://gitlab.com/desktop-frameworks/utils.git)
* DFL XDG (https://gitlab.com/desktop-frameworks/xdg.git)
* LibDesQ (https://gitlab.com/DesQ/libdesq.git)
* LibDesQ UI (https://gitlab.com/DesQ/libdesqui.git)


### Notes for compiling (Qt5) - linux:

- Install all the dependencies
- Download the sources
  * Git: `git clone https://gitlab.com/DesQ/DesQUtils/ViewInfo.git DesQViewInfo`
- Enter the `DesQViewInfo` folder
  * `cd DesQViewInfo`
- Configure the project - we use meson for project management
  * `meson setup .build --prefix=/usr --buildtype=release`
- Compile and install - we use ninja
  * `ninja -C .build -k 0 -j $(nproc) && sudo ninja -C .build install`


### Known Bugs
* The app crashes when compiled with Qt6.
* Clicking on the copy button does nothing.


### Upcoming
* Show an icon and app name for views which have a decent app-id.
* Show information about all the open views - monitor mode.
* Any other feature you request for... :)

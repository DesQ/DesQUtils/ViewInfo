/**
 * This file is a part of DesQ SNI.
 * DesQ SNI is a Widget to show SN Icons for DesQ Shell.
 * Copyright 2019-2022 Britanicus <marcusbritanicus@gmail.com>
 *
 * This file was originally taken from LXQt project
 * <https://github.com/lxqt/lxqt-panel>
 * Several modifications have been made to suit the needs of DesQ
 * Any and all bug reports are to be filed with DesQ and not LXQt.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * at your option, any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 **/

#include "WfInfo.hpp"
#include <DFApplication.hpp>


ClickLabel::ClickLabel( QWidget *parent ) : QLabel( parent ) {
    /** Horizontal expansion. Vertically, we're fixed */
    setSizePolicy( QSizePolicy( QSizePolicy::MinimumExpanding, QSizePolicy::Fixed ) );
}


void ClickLabel::mousePressEvent( QMouseEvent *mEvent ) {
    if ( mEvent->button() == Qt::LeftButton ) {
        mPressed = true;
    }

    QLabel::mousePressEvent( mEvent );
}


void ClickLabel::mouseReleaseEvent( QMouseEvent *mEvent ) {
    if ( mPressed == true ) {
        mPressed = false;

        /** Copy the text of this label to the clipboard */
        qApp->clipboard()->setText( text() );

        emit extendTime();
    }

    QLabel::mouseReleaseEvent( mEvent );
}


DesQ::ViewInfo::ViewInfo() {
    /** So that we can use it in signals */
    qRegisterMetaType<WQt::WayfireViewInfo>();

    setAttribute( Qt::WA_TranslucentBackground );
    setWindowFlags( Qt::FramelessWindowHint | Qt::BypassWindowManagerHint );

    copyBtn = new QToolButton( this );
    copyBtn->setIcon( QIcon::fromTheme( "edit-copy" ) );
    copyBtn->setAutoRaise( true );

    connect(
        copyBtn, &QToolButton::clicked, [ = ] () {
            qApp->clipboard()->setText( mCurrentInfo );
        }
    );

    setFixedWidth( 270 );

    hideTimer = new QTimer( this );
    hideTimer->setSingleShot( false );

    connect(
        hideTimer, &QTimer::timeout, [ this ] () {
            mRemainingTime = 0;
            close();
        }
    );

    mAppIdLbl      = new ClickLabel( this );
    mTitleLbl      = new ClickLabel( this );
    mUuidLbl       = new ClickLabel( this );
    mOutputLbl     = new ClickLabel( this );
    mPidLbl        = new ClickLabel( this );
    mWorkspaceLbl  = new ClickLabel( this );
    mRoleLbl       = new ClickLabel( this );
    mGeometryLbl   = new ClickLabel( this );
    mIsXwaylandLbl = new ClickLabel( this );
    mHasFocusLbl   = new ClickLabel( this );

    connect(
        mAppIdLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mTitleLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mUuidLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mOutputLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mPidLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mWorkspaceLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mRoleLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mGeometryLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mIsXwaylandLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );

    connect(
        mHasFocusLbl, &ClickLabel::extendTime, this, [ this ] () {
            mRemainingTime += 2000;
        }
    );


    QGridLayout *baseLyt = new QGridLayout();
    baseLyt->setContentsMargins( QMargins( 10, 10, 10, 10 ) );
    baseLyt->setSpacing( 5 );

    baseLyt->addWidget( new QLabel( "AppID:  ", this ),       0, 0 );
    baseLyt->addWidget( mAppIdLbl,                            0, 1 );
    baseLyt->addWidget( copyBtn,                              0, 1, Qt::AlignVCenter | Qt::AlignRight );

    baseLyt->addWidget( new QLabel( "Title:  ", this ),       1, 0 );
    baseLyt->addWidget( mTitleLbl,                            1, 1 );

    baseLyt->addWidget( new QLabel( "UUID:  ", this ),        2, 0 );
    baseLyt->addWidget( mUuidLbl,                             2, 1 );

    baseLyt->addWidget( new QLabel( "Output:  ", this ),      3, 0 );
    baseLyt->addWidget( mOutputLbl,                           3, 1 );

    baseLyt->addWidget( new QLabel( "Pid:  ", this ),         4, 0 );
    baseLyt->addWidget( mPidLbl,                              4, 1 );

    baseLyt->addWidget( new QLabel( "Workspace:  ", this ),   5, 0 );
    baseLyt->addWidget( mWorkspaceLbl,                        5, 1 );

    baseLyt->addWidget( new QLabel( "Geometry:  ", this ),    6, 0 );
    baseLyt->addWidget( mGeometryLbl,                         6, 1 );

    baseLyt->addWidget( new QLabel( "Role:  ", this ),        7, 0 );
    baseLyt->addWidget( mRoleLbl,                             7, 1 );

    baseLyt->addWidget( new QLabel( "Native View:  ", this ), 8, 0 );
    baseLyt->addWidget( mIsXwaylandLbl,                       8, 1 );

    baseLyt->addWidget( new QLabel( "Has Focus:  ", this ),   9, 0 );
    baseLyt->addWidget( mHasFocusLbl,                         9, 1 );

    setLayout( baseLyt );

    /** Get the WQt::WayfireViewInfo object when the user clicks a view */
    connect(
        wfInfo, &WQt::WayfireInformation::viewInfo, [ = ] ( WQt::WayfireViewInfo info ) {
            mAppIdLbl->setText( info.appId );
            mTitleLbl->setText( info.title );
            mUuidLbl->setText( QString::number( info.uuid ) );
            mOutputLbl->setText( QString( "%1 (id: %2)" ).arg( info.output ).arg( info.outputId ) );
            mPidLbl->setText( QString::number( info.pid ) );
            mWorkspaceLbl->setText( QString( "(%1, %2)" ).arg( info.workspace.y() ).arg( info.workspace.x() ) );
            mRoleLbl->setText( info.role );
            mGeometryLbl->setText( QString( "%1, %2 %3 x %4" ).arg( info.position.x() ).arg( info.position.y() ).arg( info.size.width() ).arg( info.size.height() ) );
            mIsXwaylandLbl->setText( info.isXwayland ? "No" : "Yes" );
            mHasFocusLbl->setText( info.hasFocus ? "true" : "false" );

            if ( mClientFD != -1 ) {
                mCurrentInfo  = QString();
                mCurrentInfo += QString( "AppID:        %1\n" ).arg( info.appId );
                mCurrentInfo += QString( "Title:        %1\n" ).arg( info.title );
                mCurrentInfo += QString( "UUID:         %1\n" ).arg( info.uuid );
                mCurrentInfo += QString( "Output:       %1 (id: %2)\n" ).arg( info.output ).arg( info.outputId );
                mCurrentInfo += QString( "Pid:          %1\n" ).arg( info.pid );
                mCurrentInfo += QString( "Workspace:    (%1, %2)\n" ).arg( info.workspace.y() ).arg( info.workspace.x() );
                mCurrentInfo += QString( "Geometry:     %1\n" ).arg( info.role );
                mCurrentInfo += QString( "Role:         %1, %2 %3 x %4\n" ).arg( info.position.x() ).arg( info.position.y() ).arg( info.size.width() ).arg( info.size.height() );
                mCurrentInfo += QString( "Native View:  %1\n" ).arg( info.isXwayland ? "No" : "Yes" );
                mCurrentInfo += QString( "Has Focus:    %1\n" ).arg( info.hasFocus ? "true" : "false" );

                qApp->messageClient( mCurrentInfo, mClientFD );

                mClientFD = -1;
            }

            /** Show the widget if it's not visible */
            if ( isVisible() == false ) {
                show();
            }

            /** Restart the timer */
            hideTimer->start( 5000 );
        }
    );
}


DesQ::ViewInfo::~ViewInfo() {
    delete mAppIdLbl;
    delete mTitleLbl;
    delete mUuidLbl;
    delete mOutputLbl;
    delete mPidLbl;
    delete mWorkspaceLbl;
    delete mRoleLbl;
    delete mGeometryLbl;
    delete mIsXwaylandLbl;
    delete mHasFocusLbl;

    delete mLyrSurf;
    delete hideTimer;
}


void DesQ::ViewInfo::show() {
    if ( mLyrSurf != nullptr ) {
        close();
    }

    QWidget::show();

    /** We want it above event fullscreen views */
    WQt::LayerShell::LayerType lyr = WQt::LayerShell::Overlay;

    /** Create the layer surface */
    mLyrSurf = wlRegistry->layerShell()->getLayerSurface( windowHandle(), nullptr, lyr, "desq-viewinfo" );

    mLyrSurf->setAnchors( WQt::LayerSurface::Top | WQt::LayerSurface::Right );

    /** Size of our surface */
    mLyrSurf->setSurfaceSize( size() );

    /** We'll keep a gap of 18 in on the left and top */
    mLyrSurf->setMargins( QMargins( 0, 10, 10, 0 ) );

    /** Move this for other layer shell surfaces */
    mLyrSurf->setExclusiveZone( 0 );

    /** We may need keyboard interaction. */
    mLyrSurf->setKeyboardInteractivity( WQt::LayerSurface::NoFocus );

    /** Commit to our choices */
    mLyrSurf->apply();
}


void DesQ::ViewInfo::hide() {
    if ( mLyrSurf != nullptr ) {
        delete mLyrSurf;
        mLyrSurf = nullptr;
    }

    QWidget::hide();
}


void DesQ::ViewInfo::close() {
    hide();

    QWidget::close();
    qApp->quit();
}


void DesQ::ViewInfo::handleMessages( QString message, int fd ) {
    if ( message == "view-info" ) {
        mClientFD = fd;
        wfInfo->getViewInfo();
    }
}


void DesQ::ViewInfo::pauseTimer() {
    mRemainingTime = hideTimer->remainingTime();
    hideTimer->stop();
}


void DesQ::ViewInfo::resumeTimer() {
    hideTimer->start( mRemainingTime );
}


void DesQ::ViewInfo::enterEvent( QMouseEnterEvent *mEvent ) {
    if ( hideTimer->isActive() ) {
        pauseTimer();
    }

    QWidget::enterEvent( mEvent );
}


void DesQ::ViewInfo::leaveEvent( QEvent *mEvent ) {
    if ( mRemainingTime ) {
        resumeTimer();
    }

    QWidget::leaveEvent( mEvent );
}


void DesQ::ViewInfo::paintEvent( QPaintEvent *pEvent ) {
    QPainter painter( this );

    painter.setRenderHints( QPainter::Antialiasing | QPainter::TextAntialiasing );

    painter.setPen( QPen( palette().color( QPalette::Window ), 1.0 ) );
    painter.setBrush( QColor( 0, 0, 0, 180 ) );

    painter.drawRoundedRect( QRectF( rect() ).adjusted( 0.5, 0.5, -0.5, -0.5 ), 5.0, 5.0 );

    painter.end();

    QWidget::paintEvent( pEvent );
}
